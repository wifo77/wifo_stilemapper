/*
	Wifo's Tile Mapper
	bY David Miguelez Garmon(2018)
*/

#include "WTMMap.h"

namespace WTM
{
	WTMMap::WTMMap(int screenWidth, int screenHeight, int levelWidth, int levelHeight, int tileWidth, int tileHeight,
		const char* pathTiles)
	{
		//Give all values
		SCREEN_WIDTH = screenWidth;
		SCREEN_HEIGHT = screenHeight;

		LEVEL_WIDTH = levelWidth;
		LEVEL_HEIGHT = levelHeight;

		TILE_WIDTH = tileWidth;
		TILE_HEIGHT = tileHeight;

		PATH_TILES = pathTiles;
	}

	void WTMMap::Start(int argc, char* args[])
	{
		//Read arguments command line
		if (ReadArgument(argc, args))
		{
			//Start up SDL and create window
			if (!Init())
			{
				printf("Failed to initialize!\n");
			}
			else
			{

				//Load media
				if (!LoadMedia())
				{
					printf("Failed to load media!\n");
				}
				else
				{
					//Main loop flag
					bool quit = false;

					//Event handler
					SDL_Event e;

					//Level camera
					SDL_Rect camera = { 0, 0, SCREEN_WIDTH, SCREEN_HEIGHT };

					//While application is running
					while (!quit)
					{
						//Handle events on queue
						while (SDL_PollEvent(&e) != 0)
						{
							//User requests quit
							if (e.type == SDL_QUIT)
							{
								quit = true;
							}
						}

						//Clear screen
						SDL_SetRenderDrawColor(gRenderer, 0xFF, 0xFF, 0xFF, 0xFF);
						SDL_RenderClear(gRenderer);

						//Render level
						for (int i = 0; i < (int)cellSet.size(); ++i)
						{
							cellSet[i].Render(camera, gRenderer);
						}

						//Update screen
						SDL_RenderPresent(gRenderer);
					}
				}

				//Free resources and close SDL
				Close();
			}
		}
		else
		{
			printf("Failed to read arguments!\n");
		}
	}

	bool WTMMap::ReadArgument(int argc, char* args[])
	{
		//Flag to read argument
		bool canStart = false;

		//Read paht of map and texture
		if (argc > 3)
		{
			if (args[1] == "0")
			{
				//0 is the ID of action: Path of map and after path texture
				PATH_MAP = args[2];
				PATH_TEXTURE = args[3];
				canStart = true;
			}
			else if (args[1] == "1")
			{
				//1 is the ID of action: Path of texture and after path map
				PATH_MAP = args[3];
				PATH_TEXTURE = args[2];
				canStart = true;
			}
			else
			{
				//Default values if ID is incorrect
				PATH_MAP = "Resources/Map.xml";
				PATH_TEXTURE = "Resources/tiles.png";
				canStart = true;
			}
		}
		//Read only a path, of map or texture
		else if (argc > 2)
		{
			if (args[1] == "2")
			{
				//2 is the ID of action: Path of map
				PATH_MAP = args[2];
				PATH_TEXTURE = "Resources/tiles.png";
				canStart = true;
			}
			else if (args[1] == "3")
			{
				//2 is the ID of action: Path of texture
				PATH_MAP = "Resources/Map.xml";
				PATH_TEXTURE = args[2];
				canStart = true;
			}
			else
			{
				//Default values if ID is incorrect
				PATH_MAP = "Resources/Map.xml";
				PATH_TEXTURE = "Resources/tiles.png";
				canStart = true;
			}
		}
		else
		{
			//Default values if no argument
			PATH_MAP = "Resources/Map.xml";
			PATH_TEXTURE = "Resources/tiles.png";
			canStart = true;
		}

		//Return flag
		return canStart;
	}

	void WTMMap::SaveTiles()
	{
		//Write xml file
		xml_document<> doc;
		xml_node<>* decl = doc.allocate_node(node_declaration);

		//Create header
		decl->append_attribute(doc.allocate_attribute("version", "1.0"));
		decl->append_attribute(doc.allocate_attribute("encoding", "utf-8"));
		doc.append_node(decl);

		//Root node
		xml_node<>* root = doc.allocate_node(node_element, "Tiles");
		doc.append_node(root);
		xml_node<>* child = nullptr;

		//Serialize all tiles
		for (unsigned int i = 0; i < gTileClips.size(); i++)
		{
			child = doc.allocate_node(node_element, "Tile");
			child->append_attribute(doc.allocate_attribute("x", ConvertIntToChar(gTileClips[i].GetBox().x)));
			child->append_attribute(doc.allocate_attribute("y", ConvertIntToChar(gTileClips[i].GetBox().y)));
			child->append_attribute(doc.allocate_attribute("width", ConvertIntToChar(gTileClips[i].GetBox().w)));
			child->append_attribute(doc.allocate_attribute("height", ConvertIntToChar(gTileClips[i].GetBox().h)));
			child->append_attribute(doc.allocate_attribute("tileType", ConvertIntToChar(gTileClips[i].GetType())));
			root->append_node(child);

		}

		//Convert doc to string
		std::string xml_as_string;
		rapidxml::print(std::back_inserter(xml_as_string), doc);

		//Save to file
		std::ofstream file_stored(PATH_TILES);
		file_stored << xml_as_string;
		file_stored.close();
		doc.clear();
	}

	void WTMMap::SaveMap()
	{
		//Write xml file
		xml_document<> doc;
		xml_node<>* decl = doc.allocate_node(node_declaration);

		//Create header
		decl->append_attribute(doc.allocate_attribute("version", "1.0"));
		decl->append_attribute(doc.allocate_attribute("encoding", "utf-8"));
		doc.append_node(decl);

		//Root node
		xml_node<>* root = doc.allocate_node(node_element, "Map");
		doc.append_node(root);
		xml_node<>* child = nullptr;

		//Serialize all cells
		for (unsigned int i = 0; i < cellSet.size(); i++)
		{
			child = doc.allocate_node(node_element, "Cell");
			child->append_attribute(doc.allocate_attribute("typeOfTile", ConvertIntToChar(cellSet[i].GetType())));
			root->append_node(child);
		}

		//Convert doc to string
		std::string xml_as_string;
		rapidxml::print(std::back_inserter(xml_as_string), doc);

		//Save to file
		std::ofstream file_stored(PATH_MAP);
		file_stored << xml_as_string;
		file_stored.close();
		doc.clear();
	}

	char* WTMMap::ConvertIntToChar(int i)
	{
		//Cast int to string and copy string in char array
		string value = to_string(i);
		int p = value.length() + 1;
		char* c = new char[p];
		strcpy(c, value.c_str());

		return c;
	}

	bool WTMMap::SetTiles()
	{
		//Read the xml file into a vector
		xml_document<> doc;
		xml_node<> * root_node;

		ifstream file(PATH_TILES);
		if (file.good())
		{
			vector<char> buffer((istreambuf_iterator<char>(file)), istreambuf_iterator<char>());
			buffer.push_back('\0');
			//Parse the buffer using the xml file parsing library into doc 
			doc.parse<0>(&buffer[0]);
			//Find our root node
			root_node = doc.first_node("Tiles");
			//Read all attributes
			for (xml_node<> * node = root_node->first_node("Tile"); node; node = node->next_sibling())
			{
				int x = stoi(node->first_attribute("x")->value());
				int y = stoi(node->first_attribute("y")->value());
				int width = stoi(node->first_attribute("width")->value());
				int height = stoi(node->first_attribute("height")->value());
				int tileType = stoi(node->first_attribute("tileType")->value());

				gTileClips.push_back(WTMTile(x, y, width, height, tileType, &gTileTexture));
			}
		}
		else
		{
			cout << "File don't exist" << endl;
		}

		return true;
	}

	bool WTMMap::SetCells()
	{
		//Success flag
		bool tilesLoaded = true;

		if (SetTiles())
		{
			//The tile offsets
			int x = 0, y = 0;

			std::vector<int>types;


			//Read the xml file into a vector
			xml_document<> doc;
			xml_node<> * root_node;

			ifstream file(PATH_MAP);
			if (file.good())
			{

				vector<char> buffer((istreambuf_iterator<char>(file)), istreambuf_iterator<char>());
				buffer.push_back('\0');
				//Parse the buffer using the xml file parsing library into doc 
				doc.parse<0>(&buffer[0]);
				//Find our root node
				root_node = doc.first_node("Map");
				//Read attribute
				for (xml_node<> * node = root_node->first_node("Cell"); node; node = node->next_sibling())
				{
					types.push_back(stoi(node->first_attribute("typeOfTile")->value()));
				}

				//Initialize the tiles
				for (int i = 0; i < (int)types.size(); ++i)
				{
					//Give value of type 
					int tileType = types[i];

					//If the number is a valid tile number
					if ((tileType >= 0) && (tileType < (int)gTileClips.size()))
					{
						cellSet.push_back(WTMCell(x, y, TILE_WIDTH, TILE_HEIGHT, GetTile(tileType)));
					}
					//If we don't recognize the tile type
					else
					{
						//Stop loading map
						printf("Error loading map: Invalid tile type at %d!\n", i);
						tilesLoaded = false;
						break;
					}

					//Move to next tile spot
					x += TILE_WIDTH;

					//If we've gone too far
					if (x >= LEVEL_WIDTH)
					{
						//Move back
						x = 0;

						//Move to the next row
						y += TILE_HEIGHT;
					}
				}
			}
		}
		return tilesLoaded;
	}

	WTMTile* WTMMap::GetTile(int type)
	{
		//Go through the vector in search of the right type of tile
		for (unsigned int i = 0; i < gTileClips.size(); i++)
		{
			//Check if it is correct
			if (gTileClips[i].GetType() == type)
			{
				//Return the tile
				return &gTileClips[i];
			}
		}
		//If it does not find it, it returns a null pointer
		return nullptr;
	}

	bool WTMMap::Init()
	{
		//Initialization flag
		bool success = true;

		//Initialize SDL
		if (SDL_Init(SDL_INIT_VIDEO) < 0)
		{
			printf("SDL could not initialize! SDL Error: %s\n", SDL_GetError());
			success = false;
		}
		else
		{
			//Set texture filtering to linear
			if (!SDL_SetHint(SDL_HINT_RENDER_SCALE_QUALITY, "1"))
			{
				printf("Warning: Linear texture filtering not enabled!");
			}

			//Create window
			gWindow = SDL_CreateWindow("Wifo_sTileMapper", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, SCREEN_WIDTH, SCREEN_HEIGHT, SDL_WINDOW_SHOWN);
			if (gWindow == NULL)
			{
				printf("Window could not be created! SDL Error: %s\n", SDL_GetError());
				success = false;
			}
			else
			{
				//Create renderer for window
				gRenderer = SDL_CreateRenderer(gWindow, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
				if (gRenderer == NULL)
				{
					printf("Renderer could not be created! SDL Error: %s\n", SDL_GetError());
					success = false;
				}
				else
				{
					//Initialize renderer color
					SDL_SetRenderDrawColor(gRenderer, 0xFF, 0xFF, 0xFF, 0xFF);

					//Initialize PNG loading
					int imgFlags = IMG_INIT_PNG;
					if (!(IMG_Init(imgFlags) & imgFlags))
					{
						printf("SDL_image could not initialize! SDL_image Error: %s\n", IMG_GetError());
						success = false;
					}
				}
			}
		}

		return success;
	}

	bool WTMMap::LoadMedia()
	{
		//Loading success flag
		bool success = true;

		//Load tile texture
		if (!gTileTexture.LoadFromFile(PATH_TEXTURE, gRenderer))
		{
			printf("Failed to load tile set texture!\n");
			success = false;
		}
		else
		{
			//Load cells map
			if (!SetCells())
			{
				printf("Failed to load tile set!\n");
				success = false;
			}
		}

		return success;
	}

	void WTMMap::Close()
	{
		cellSet.clear();

		//Free loaded images
		gTileTexture.Free();

		//Destroy window	
		SDL_DestroyRenderer(gRenderer);
		SDL_DestroyWindow(gWindow);
		gWindow = NULL;
		gRenderer = NULL;

		//Quit SDL subsystems
		IMG_Quit();
		SDL_Quit();
	}
}