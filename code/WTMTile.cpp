/*
	Wifo's Tile Mapper
	bY David Miguelez Garmon(2018)
*/

#include "WTMTile.h"

namespace WTM
{

	WTMTile::WTMTile(int x, int y,int width, int height, int tileType, WTMTexture* texture)
	{
		//Get the offsets
		mBox.x = x;
		mBox.y = y;

		//Set the collision box
		mBox.w = width;
		mBox.h = height;

		//Set the tile type
		mType = tileType;

		//Set the texture to use
		textureUseThisTile = texture;
	}

	void WTMTile::Render(int x, int y, SDL_Renderer* render)
	{		
		//Show the tile
		textureUseThisTile->Render(render, x, y, &mBox);		
	}

	int WTMTile::GetType()
	{
		//Give type of tile
		return mType;
	}

	SDL_Rect WTMTile::GetBox()
	{
		//Get all info of position and dimensions
		return mBox;
	}	
}