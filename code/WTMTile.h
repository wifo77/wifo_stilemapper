#ifndef WTM_TILE_HEADER
#define WTM_TILE_HEADER

#include <SDL.h>
#include "WTMTexture.h"

namespace WTM
{
	//The tile
	class WTMTile
	{
	public:
		//Initializes position and type
		WTMTile(int x, int y, const int width, const int height, int tileType, WTMTexture* texture);

		//Shows the tile
		void Render(int x, int y, SDL_Renderer* render);

		//Get the tile type
		int GetType();

		//Get the collision box
		SDL_Rect GetBox();

	private:
		//The attributes of the tile
		SDL_Rect mBox;
		//Pointer of texture of this type tile
		WTMTexture* textureUseThisTile;

		//The tile type
		int mType;
	};
}
#endif