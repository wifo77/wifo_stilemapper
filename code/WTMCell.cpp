/*
	Wifo's Tile Mapper
	bY David Miguelez Garmon(2018)
*/

#include "WTMCell.h"

namespace WTM
{
	WTMCell::WTMCell(int x, int y, int width, int height, WTMTile* tileType)
	{
		//Add the position and its dimensions
		boxPosition.x = x;
		boxPosition.y = y;
		boxPosition.w = width;
		boxPosition.h = height;

		//Gives the pointer tile
		typeOfTile = tileType;
	}

	void WTMCell::Render(SDL_Rect& camera, SDL_Renderer* render)
	{
		//If the tile is on screen
		if (CheckCollision(camera, boxPosition))
		{
			//Render cell
			typeOfTile->Render(boxPosition.x - camera.x, boxPosition.y - camera.y, render);
		}
	}

	bool WTMCell::CheckCollision(SDL_Rect a, SDL_Rect b)
	{
		//The sides of the rectangles
		int leftA, leftB;
		int rightA, rightB;
		int topA, topB;
		int bottomA, bottomB;

		//Calculate the sides of rect A
		leftA = a.x;
		rightA = a.x + a.w;
		topA = a.y;
		bottomA = a.y + a.h;

		//Calculate the sides of rect B
		leftB = b.x;
		rightB = b.x + b.w;
		topB = b.y;
		bottomB = b.y + b.h;

		//If any of the sides from A are outside of B
		if (bottomA <= topB)
		{
			return false;
		}

		if (topA >= bottomB)
		{
			return false;
		}

		if (rightA <= leftB)
		{
			return false;
		}

		if (leftA >= rightB)
		{
			return false;
		}

		//If none of the sides from A are outside B
		return true;
	}

	int WTMCell::GetType()
	{
		return typeOfTile->GetType();
	}
}