/*
	Wifo's Tile Mapper
	bY David Miguelez Garmon(2018)
*/
#pragma once

#ifdef WTM_MAP_EXPORTS
#define WTM_MAP_API __declspec(dllexport) 
#else
#define WTM_MAP_API __declspec(dllimport) 
#endif

#ifndef WTM_MAP_HEADER
#define WTM_MAP_HEADER

#include <SDL.h>
#include <stdio.h>
#include <string>
#include <fstream>
#include <iostream>
#include <vector>

#include <sstream>
#include "rapidxml\rapidxml.hpp"
#include "rapidxml\rapidxml_print.hpp"


#include "WTMTexture.h"
#include "WTMTile.h"
#include "WTMCell.h"


using namespace rapidxml;
using namespace std;

namespace WTM
{
	class WTMMap
	{

	public:
		//Initializes variables
		WTM_MAP_API WTMMap(int screenWidth, int screenHeight, int levelWidth, int levelHeight, int tileWidth, int tileHeight,const char* pathTiles);
		//It's the core of the map. It's responsible for loading and maintaining the map
		WTM_MAP_API void Start(int argc, char* args[]);
		//Cells give type of tile they need.
		WTMTile* GetTile(int type);
		
		//Save the different types of tiles
		void SaveTiles();
		//Save the different cells of map
		void SaveMap();
		
	private:

		//Screen dimension constants
		int SCREEN_WIDTH;
		int SCREEN_HEIGHT;

		//The dimensions of the level
		int LEVEL_WIDTH;
		int LEVEL_HEIGHT;

		//Tile constants
		int TILE_WIDTH;
		int TILE_HEIGHT;
			
		//Paths of load resources	
		const char* PATH_MAP;
		const char* PATH_TEXTURE;
		const char* PATH_TILES;

		//Starts up SDL and creates window
		bool Init();

		//Loads media
		bool LoadMedia();

		//Frees media and shuts down SDL
		void Close();

		//Sets diferent tiles to use
		bool SetTiles();

		//Sets cells from tile map
		bool SetCells();

		//Sets paths of map and texture
		bool ReadArgument(int argc, char* args[]);

		//The window we'll be rendering to
		SDL_Window* gWindow = NULL;

		//The window renderer
		SDL_Renderer* gRenderer = NULL;

		//Scene textures
		WTMTexture gTileTexture;
		//All types of tiles
		std::vector<WTMTile> gTileClips;
		std::vector<WTMCell> cellSet;

		char* ConvertIntToChar(int i);
	};
}
#endif