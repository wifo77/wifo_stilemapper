/*
	Wifo's Tile Mapper
	bY David Miguelez Garmon(2018)
*/

#ifndef WTM_CELL_HEADER
#define WTM_CELL_HEADER

#include <SDL.h>
#include "WTMTile.h" 

namespace WTM
{
	class WTMCell
	{
	public:
		//Initializes variables
		WTMCell(int x, int y, int width, int height, WTMTile* tileType);
		//Render the cell with its type of tile
		void Render(SDL_Rect& camera, SDL_Renderer* render);
		int GetType();


	private:
		//Pointer of type tile
		WTMTile* typeOfTile;
		//Contains the position and dimensions of the cell
		SDL_Rect boxPosition;

		//Check if the cell is inside the camera
		bool CheckCollision(SDL_Rect a, SDL_Rect b);
	};
}
#endif