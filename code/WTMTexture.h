/*
	Wifo's Tile Mapper
	bY David Miguelez Garmon(2018)
*/

#ifdef WTM_TEXTURE_EXPORTS
#define WTM_TEXTURE_API __declspec(dllexport) 
#else
#define WTM_TEXTURE_API __declspec(dllimport) 
#endif

#ifndef WTM_TEXTURE_HEADER
#define WTM_TEXTURE_HEADER

//Using SDL, SDL_image and strings
#include <SDL.h>
#include <SDL_image.h>
#include <string>

namespace WTM
{
	class WTMTexture
	{
	public:
		//Initializes variables
		WTM_TEXTURE_API WTMTexture();

		//Deallocates memory
		WTM_TEXTURE_API ~WTMTexture();

		//Loads image at specified path
		bool LoadFromFile(std::string path, SDL_Renderer* renderer);

		//Deallocates texture
		void Free();

		//Set color modulation
		void SetColor(Uint8 red, Uint8 green, Uint8 blue);

		//Set blending
		void SetBlendMode(SDL_BlendMode blending);

		//Set alpha modulation
		void SetAlpha(Uint8 alpha);

		//Renders texture at given point
		void Render(SDL_Renderer* renderer, int x, int y, SDL_Rect* clip = NULL, double angle = 0.0, SDL_Point* center = NULL, SDL_RendererFlip flip = SDL_FLIP_NONE);

		//Gets image dimensions
		int GetWidth();
		int GetHeight();

	private:
		//The actual hardware texture
		SDL_Texture * mTexture;

		//Image dimensions
		int mWidth;
		int mHeight;
	};
}
#endif