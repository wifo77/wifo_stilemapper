#include "WTMMap.h"


int main(int argc, char* args[])
{
	//Load and show the map
	WTM::WTMMap* map = new WTM::WTMMap(640, 480, 1280, 960, 80, 80, "Resources/TilesStored.xml");
	map->Start(argc, args);

	delete map;

	return 0;
}